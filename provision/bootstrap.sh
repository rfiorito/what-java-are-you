#!/usr/bin/env bash
set -e

# Download Oracle jdk-8.u91

# Install jdk
sudo yum -y localinstall /vagrant/jdk_source/OracleJDK/jdk-8u91-linux-x64.rpm

# Donwnlod wildfly
WILDFLY_VERSION=9.0.2.Final
wget -O /vagrant/wildfly_source/ /https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz
# Create user wildfly
groupadd -r wildfly
useradd -g wildfly -d /home/wildfly -s /bin/bash  -c "Wildfly user" wildfly
# Install wildfly
mkdir -p /opt/wildfly
tar -xzvf /vagrant/wildfly_source/wildfly-$WILDFLY_VERSION.tar.gz -C /opt/wildfly/
ln -s /opt/wildfly/wildfly-$WILDFLY_VERSION /opt/wildfly/current
mkdir /opt/wildfly/bin
cp /opt/wildfly/current/bin/init.d/wildfly* /opt/wildfly/bin/
sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'
chown -R wildfly:wildfly /opt/wildfly
cp /vagrant/etc/systemd/system/wildfly.service /etc/systemd/system/wildfly.service
#mkdir -p /etc/wildfly
#cp /opt/wildfly/current/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/
sudo systemctl daemon-reload
sudo systemctl start wildfly
sudo systemctl enable wildfly
sudo /opt/wildfly/current/bin/add-user.sh admin admin --silent
