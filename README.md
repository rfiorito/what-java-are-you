# What java are you

Vagrant Dev Environment on CentOS7 with Wildfly 9.0.2  and Multi jdk Version for local user (Default Oracle jdk-8-u91)

Before moving ahead with this starter environment you'll need to have a few things installed on your computer:
* [VirtualBox](https://www.virtualbox.org/) - Free tool that allows you to run virtual machines on your computer.
* [Vagrant](https://www.vagrantup.com/) - Free tool that automates the creation of development environments within a virtual machine.

Once you've installed VirtualBox and Vagrant on your computer you're ready to continue:
1. Open a command-line (Terminal on Mac/Linux, PowerShell or Git Bash on Windows)
2. cd into this project's root folder
3. Run `vagrant up`
4. Go grab a coffee, it will take a few minutes
